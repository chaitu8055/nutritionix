import { Component } from '@angular/core';
import { HealthService } from '../health.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  constructor(private service: HealthService)
  {

  }
logout():boolean
{
  return this.service.getUserLoggedStatus();
}
login():boolean
{
  return this.service.getUserLoggedStatus();
}
}
