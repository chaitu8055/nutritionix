import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HealthService {
  isUserLogged: boolean;

  constructor(private http: HttpClient) {
    this.isUserLogged = false;
  }
  setUserLoggedIn() {
    this.isUserLogged = true;
  }
  
  setUserLoggedOut() {
    this.isUserLogged = false;
  }
  
  getUserLoggedStatus(): boolean {
    return this.isUserLogged;
  }



  registerEmp(employee:any){
    return this.http.post('registerCustomer',employee);
  }

  loginEmp(employee:any){
    return this.http.get('loginByEmail/'+employee.username+"/"+employee.password).toPromise();
  }


}
