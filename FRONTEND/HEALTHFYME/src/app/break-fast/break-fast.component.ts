import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { HealthService } from '../health.service';
import { RouterModule } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
@Component({
  selector: 'app-break-fast',
  templateUrl: './break-fast.component.html',
  styleUrls: ['./break-fast.component.css']
})
export class BreakFastComponent {
  BreakfastItems: any;
  constructor() {
    this.BreakfastItems = [
      { name: "Boiled egg", fat: 6.11, carbs: 0.63, protien: 6.14, calories: 84, image: "assets/images/BoiledEgg.jpg" },
      { name: "OatMeal", fat: 2.29, carbs: 37.81, protien: 6.18, calories: 190, image: "/assets/images/breakfast/oatmeal.jpg" },
      { name: "Pan Cake", fat: 2.65, carbs: 11.23, protien: 2.70, calories: 80, image: "/assets/images/brealfast/pan cake.jpg" },
      { name: "Raspberry smoothie", fat: 2.01, carbs: 33, protien: 10, calories: 160, image: "/assets/images/femaleAvatar.webp" },
      { name: "Toasted Bread", fat: 1.15, carbs: 13.03, protien: 2.50, calories: 71, image: "/assets/images/femaleAvatar.webp" },
      { name: "Idly", fat: 6.11, carbs: 0.63, protien: 6.14, calories: 84, image: "/assets/images/femaleAvatar.webp" },
      { name: "Milk", fat: 10.3, carbs: 11, protien: 8, calories: 168, image: "/assets/images/femaleAvatar.webp" },
      { name: "White Sauce Pasta", fat: 6.11, carbs: 0.63, protien: 6.14, calories: 84, image: "/assets/images/femaleAvatar.webp" },
      { name: "Banana Smoothie", fat: 7.2, carbs: 44.4, protien: 7.8, calories: 174, image: "/assets/images/femaleAvatar.webp" }
    ]
  }
}