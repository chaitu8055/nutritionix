import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './Home/Home.component'
import { CalorieCalculatorComponent } from './calories-analysis/calories-analysis.component';
import { LogoutComponent } from './logout/logout.component';
import { BreakFastComponent } from './break-fast/break-fast.component';
import { LunchComponent } from './lunch/lunch.component';
const routes: Routes = [
  // {path:"", component:LoginComponent},
  // {path:"", component:HomeComponent},
  {path:"login", component:LoginComponent},
  {path:"register", component:RegisterComponent},
  {path:"calories-analysis",component:CalorieCalculatorComponent},
  {path:"logout",component:LogoutComponent},
  {path:"BreakFast",component:BreakFastComponent},
  {path:"lunch",component:LunchComponent}
  
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }