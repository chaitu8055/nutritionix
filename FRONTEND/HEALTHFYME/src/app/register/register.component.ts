import { Component } from '@angular/core';
import { HealthService } from '../health.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  employee: any;
  constructor(private service:HealthService,private router: Router){
    this.employee = {   
        firstName:"",
        lastName:"",
        gender:"",
        phone:"",
        emailId:"",
        password:""
    };
    }
    validateReg(Employee: any){
      alert('succes');
      console.log(Employee);
      
      this.employee.firstName = Employee.firstname;
      
      this.employee.lastName = Employee.lastname;
      
      this.employee.gender = Employee.gender;
      
      this.employee.phone = Employee.phoneNumber;
      
      this.employee.emailId = Employee.emailId;
      
      this.employee.password = Employee.password;

      console.log(this.employee);
      
      this.service.registerEmp(this.employee).subscribe((data:any) =>{
        console.log(data);
        alert('succes');
        
      });
      this.router.navigate(['login']);
    }
    isPasswordValid(password: string): boolean {
      const uppercase = /[A-Z]/;
      if (!uppercase.test(password)) {
        return false;
      }
  
      const specialCharacter = /[$&+,:;=?@#|'<>.^*()%!-]/;
      if (!specialCharacter.test(password)) {
        return false;
      }
      const numbers = /[0-9]/;
      if (!numbers.test(password)) {
        return false;
      }
  
      return true;
    }
  
    isPhoneNumberValid(phoneNumber: string): boolean {

      const pattern = /^[6-9]\d{9}$/;
      if (!pattern.test(phoneNumber)) {
        return false;
      }
  
      return true;
  
    }
  }

 



