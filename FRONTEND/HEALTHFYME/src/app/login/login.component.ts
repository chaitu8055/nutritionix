import { Component } from '@angular/core';
import { HealthService } from '../health.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  username: any;
  password: any;


  constructor(private service: HealthService,private router:Router) {

  }

  async validateLogin(loginForm: any) {
    console.log(loginForm);

    if (loginForm.username == "HR" && loginForm.password == "HR") {
      alert("Login Success");
      this.service.setUserLoggedIn();
      // this.router.navigate(['calories-analysis']);
    }else{
    await this.service.loginEmp(loginForm).then((data:any)=>{
      console.log(data);
      this.service.setUserLoggedIn();
      this.router.navigate(['calories-analysis']);
     });    
    }
  }
}
