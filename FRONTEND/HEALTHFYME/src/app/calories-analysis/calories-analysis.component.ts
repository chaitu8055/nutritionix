import { Component } from '@angular/core';
@Component({
  selector: 'app-calorie-calculator',
  templateUrl: './calories-analysis.component.html',
  styleUrls: ['./calories-analysis.component.css']
})
export class CalorieCalculatorComponent {
  user = {
    age: 0,
    gender: 'male'
  };
  calculatedCalories: number = 0;
  caloriesCalculated: boolean = false;
  calculateCalories() {
    if (this.user.age > 0) {
      if (this.user.gender === 'male') {
        this.calculatedCalories = 66.5 + (13.75 * this.user.age);
      } else {
        this.calculatedCalories = 655.1 + (9.563 * this.user.age);
      }
      this.caloriesCalculated = true;
    }
    else {
      return alert("PLEASE ENTER A VALID AGE");
    }
  }
}