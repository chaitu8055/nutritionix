package com.DAO;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.HealthEntity;
@Service
public class HealthDao{
	
	@Autowired
	HealthRepository healthyRepository;

	 public void registerCustomer(HealthEntity healthyEntity) {
		 healthyRepository.save(healthyEntity);
		 
	 }

	public HealthEntity loginByEmail(String emailId, String password) {
		
		return healthyRepository.loginByEmail(emailId,password);
	}
  
}