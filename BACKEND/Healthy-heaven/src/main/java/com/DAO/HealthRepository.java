package com.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.HealthEntity;

@Repository
public interface HealthRepository  extends JpaRepository<HealthEntity,Integer>{
	@Query("from HealthEntity h where h.emailId= :emailId and h.password = :password")
	HealthEntity loginByEmail(@Param("emailId")String emailId,@Param("password")String password );
}