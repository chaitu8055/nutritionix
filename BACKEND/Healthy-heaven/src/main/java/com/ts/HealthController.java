package com.ts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.DAO.HealthDao;
//import com.DAO.HealthyDao;
import com.model.HealthEntity;


@RestController
public class HealthController {
	
	@Autowired
	HealthDao healthDao;
	
	@PostMapping("registerCustomer")
	public String registerCustomer(@RequestBody HealthEntity healthyEntity){
		healthDao.registerCustomer(healthyEntity);
		return "Customer Registered Succesfully";
	}
	@GetMapping("loginByEmail/{emailId}/{password}")
	public HealthEntity loginByEmail(@PathVariable("emailId") String emailId,@PathVariable("password")String password){
		return healthDao.loginByEmail(emailId,password);
	}
	
	
}